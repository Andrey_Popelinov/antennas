#pragma once
#include "afxwin.h"
#include <gdiplus.h>
#include <vector>
#include <iostream>
using namespace Gdiplus;
using namespace std;
struct Point3D { double x, y, z; };

class Paint2D : public CStatic
{

public:
	double xmin, xmax, ymin, ymax;
	int *_R;
	std::vector<std::vector<double>>* image;
	Paint2D();
	~Paint2D();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	void Update();
	REAL X(LPDRAWITEMSTRUCT RECT, float x);
	REAL Y(LPDRAWITEMSTRUCT RECT, float y);
	REAL Width(LPDRAWITEMSTRUCT RECT, float width);
	REAL Height(LPDRAWITEMSTRUCT RECT, float height);

};
class Paint : public CStatic
{

public:
	double xmin, xmax, ymin, ymax;
	int width, height;
	std::vector<Point> points;
	Paint();
	~Paint();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	void Update();
	REAL X(LPDRAWITEMSTRUCT RECT, float x);
	REAL Y(LPDRAWITEMSTRUCT RECT, float y);
	REAL Width(LPDRAWITEMSTRUCT RECT, float width);
	REAL Height(LPDRAWITEMSTRUCT RECT, float height);
	double pixelToX(double xPx);
	double pixelToY(double yPx);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};

class Paint3D
{
public:
	Paint3D();
	~Paint3D();

	double *angleX, *angleY, *zoom;
	void Draw();
	std::vector<std::vector<Point3D>> *points;

	/*������������� �������� ��������� OpenGl*/
	void InitiateOPGL(CRect & rt, CDC* pdc);
	CRect rect;
	CDC* pDC;

	/*������� ���������� ���������� ������ ���������*/
	BOOL bSetupPixelFormat();
};

