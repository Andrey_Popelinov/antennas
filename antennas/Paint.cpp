#include "stdafx.h"
#include "Paint.h"
#include <math.h>
#include <gl/GLU.h>
#include <gl/GL.h>
#include "glut.h"
using namespace Gdiplus;

ULONG_PTR token2;
ULONG_PTR token1;

Paint2D::Paint2D()
{
	Status s;
	GdiplusStartupInput input;
	s = GdiplusStartup(&token2, &input, NULL);
	if (s != Ok)
	{
		MessageBox(L"ERROR!!!", L"������", MB_ICONERROR);
	}
}


Paint2D::~Paint2D()
{
	//GdiplusShutdown(token2);
}

//���������
void Paint2D::DrawItem(LPDRAWITEMSTRUCT RECT)
{
	Graphics gr(RECT->hDC);//����
	if (image[0].size() > 0)
	{
		size_t width = image[0][0].size();
		size_t height = image->size();
		xmin = 0;
		xmax = width;
		ymin = 0;
		ymax = height;

		double max = 0;
		for (size_t i = 0; i < height; i++)
		{
			for (size_t j = 0; j < width; j++)
			{
				if (image[0][i][j] > max) max = image[0][i][j];
			}
		}

		Bitmap bmpBuffer(width, height);

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				double val = image[0][i][j] / max * 255.0;
				Color color;
				color = Color::MakeARGB(255, val, val, val);
				bmpBuffer.SetPixel(j, height - 1 - i, color);
			}
		}
		Rect rect(0, 0, RECT->rcItem.right, RECT->rcItem.bottom);
		gr.DrawImage(&bmpBuffer, rect);
	}
}


REAL Paint2D::X(LPDRAWITEMSTRUCT RECT, float x)
{
	return (REAL)(RECT->rcItem.right) / (xmax - xmin)*((x)-xmin);
}

REAL Paint2D::Y(LPDRAWITEMSTRUCT RECT, float y)
{
	return -(REAL)(RECT->rcItem.bottom) / (ymax - ymin)*((y)-ymax);
}

REAL Paint2D::Width(LPDRAWITEMSTRUCT RECT, float width)
{
	return (REAL)(RECT->rcItem.right) / (xmax - xmin)*width;
}

REAL Paint2D::Height(LPDRAWITEMSTRUCT RECT, float height)
{
	return (REAL)(RECT->rcItem.bottom) / (ymax - ymin)*height;
}

void Paint2D::Update()
{
	RedrawWindow();
}
Paint::Paint()
{
	Status s;
	GdiplusStartupInput input;
	s = GdiplusStartup(&token1, &input, NULL);
	if (s != Ok)
	{
		MessageBox(L"ERROR!!!", L"������", MB_ICONERROR);
	}
}


Paint::~Paint()
{
//	GdiplusShutdown(token1);
}
void Paint::DrawItem(LPDRAWITEMSTRUCT RECT)
{
	Graphics gr(RECT->hDC);
	Bitmap bmp(RECT->rcItem.right, RECT->rcItem.bottom, &gr);
	width = RECT->rcItem.right;
	height = RECT->rcItem.bottom;
	Graphics grBmp(&bmp);
	grBmp.Clear(Color::White);

	Pen penLines(Color::Black, 1);
	Pen penGreatLines(Color::LightGreen, 2);

	for (int i = 1; i < xmax; i++)
	{
	//	grBmp.DrawLine(&penLines, X(RECT, i), Y(RECT, ymin), X(RECT, i), Y(RECT, ymax));
	}

	for (int i = xmin + 1; i < 0; i++)
	{
		//grBmp.DrawLine(&penLines, X(RECT, i), Y(RECT, ymin), X(RECT, i), Y(RECT, ymax));
	}

	for (int i = 1; i < ymax; i++)
	{
	//	grBmp.DrawLine(&penLines, X(RECT, xmin), Y(RECT, i), X(RECT, xmax), Y(RECT, i));
	}

	for (int i = ymin + 1; i < 0; i++)
	{
	//	grBmp.DrawLine(&penLines, X(RECT, xmin), Y(RECT, i), X(RECT, xmax), Y(RECT, i));
	}

	//grBmp.DrawLine(&penGreatLines, X(RECT, xmin), Y(RECT, 0), X(RECT, xmax), Y(RECT, 0));
	//grBmp.DrawLine(&penGreatLines, X(RECT, 0), Y(RECT, ymin), X(RECT, 0), Y(RECT, ymax));

	if (!points.empty())
	{
		SolidBrush brushDot(Color::Black);
		SolidBrush brushDotb(Color::White);
		for (size_t i = 0; i < points.size(); i++)
		{
			grBmp.FillEllipse(&brushDot, X(RECT, points[i].X) - 6.f, Y(RECT, points[i].Y) - 6.f, 12.f, 12.f);
			grBmp.FillEllipse(&brushDotb, X(RECT, points[i].X) - 5.f, Y(RECT, points[i].Y) - 5.f, 10.f, 10.f);
		}
	}

	gr.DrawImage(&bmp, 0, 0);
}


REAL Paint::X(LPDRAWITEMSTRUCT RECT, float x)
{
	return (REAL)(RECT->rcItem.right) / (xmax - xmin)*((x)-xmin);
}

REAL Paint::Y(LPDRAWITEMSTRUCT RECT, float y)
{
	return -(REAL)(RECT->rcItem.bottom) / (ymax - ymin)*((y)-ymax);
}

REAL Paint::Width(LPDRAWITEMSTRUCT RECT, float width)
{
	return (REAL)(RECT->rcItem.right) / (xmax - xmin)*width;
}

REAL Paint::Height(LPDRAWITEMSTRUCT RECT, float height)
{
	return (REAL)(RECT->rcItem.bottom) / (ymax - ymin)*height;
}

void Paint::Update()
{
	RedrawWindow();
}
BEGIN_MESSAGE_MAP(Paint, CStatic)
	ON_WM_LBUTTONDOWN()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()


void Paint::OnLButtonDown(UINT nFlags, CPoint point)
{
	Point pt;
	double d0 = pixelToX(point.x), d1, doubInt;
	d1 = modf(d0, &doubInt);//���������� ���������� ����� 
	if (d1 > 0)
	{
		if (d1 < 0.5) pt.X = (int)doubInt;
		else pt.X = (int)++doubInt;
	}
	else
	{
		if (fabs(d1) < 0.5) pt.X = (int)doubInt;
		else pt.X = (int)--doubInt;
	}

	d0 = pixelToY(point.y);
	d1 = modf(d0, &doubInt);//���������� ���������� ����� 
	if (d1 > 0)
	{
		if (d1 < 0.5) pt.Y = (int)doubInt;
		else pt.Y = (int)++doubInt;
	}
	else
	{
		if (fabs(d1) < 0.5) pt.Y = (int)doubInt;
		else pt.Y = (int)--doubInt;
	}
	if (!points.empty())//�������� �� ��������
	{
		int position;
		bool check = false;
		for (size_t i = 0; i < points.size(); i++)
		{
			if (points[i].X == pt.X && points[i].Y == pt.Y)
			{
				check = true;
				position = i;
				break;
			}
		}
		if (!check) points.push_back(pt);
		else
		{
			points.erase(points.begin() + position);
		}
	}
	else points.push_back(pt);
	Update();

	CStatic::OnLButtonDown(nFlags, point);
}

double Paint::pixelToX(double xPx)
{
	return (xmax - xmin) / width * xPx + xmin;
}

double Paint::pixelToY(double yPx)
{
	return -(ymax - ymin) / height * yPx + ymax;
}

void Paint::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������

	CStatic::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
Paint3D::Paint3D()
{
}


Paint3D::~Paint3D()
{
	wglMakeCurrent(pDC->GetSafeHdc(), 0);
	wglDeleteContext(wglGetCurrentContext());
}

void Paint3D::Draw()
{
	

	{
		double width = points[0][0].size() / 2;
		glClearColor(0.5f, 0.5f, 1.f, 0.5);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-width * 2.0, width*2.0, -width * 2.0, width*2.0, -width * 2.0, width*2.0);

		glTranslated(0.f, 0.f, 0.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		glRotated(*angleX, 1, 0, 0);
		glRotated(*angleY, 0, 1, 0);
		glScaled(*zoom, *zoom, *zoom);
		glPolygonMode(GL_FRONT_AND_BACK, GL_QUADS);//
		glBegin(GL_QUADS);

		double max = 0;//����� ���������
		for (int i = 0; i < points[0].size(); i++)
		{
			for (int j = 0; j < points[0][i].size(); j++)
			{
				if (points[0][i][j].z > max) max = points[0][i][j].z;
			}
		}
		//���������������
		for (int i = 0; i < points[0].size(); i++)
		{
			for (int j = 0; j < points[0][i].size(); j++)
			{
				points[0][i][j].z = points[0][i][j].z / max * width;
			}
		}

		for (int i = 0; i < points[0].size(); i++)
		{
			for (int j = 0; j < points[0][i].size(); j++)
			{
				glColor3d(0, 0, 0);
				int iright = i + 1;
				int jup = j + 1;
				if (iright == points[0].size())//������� �������
				{
					iright -= 2;
				}
				if (jup == points[0][i].size())
				{
					jup -= 2;
				}
				glColor3f(points[0][i][j].z / max, 0, 0);
				glVertex3d(points[0][i][j].x, points[0][i][j].y, points[0][i][j].z);//1 �����
				glColor3f(points[0][iright][j].z / max, 0, 0);
				glVertex3d(points[0][iright][j].x, points[0][iright][j].y, points[0][iright][j].z);//
				glColor3f(points[0][iright][jup].z / max, 0, 0);
				glVertex3d(points[0][iright][jup].x, points[0][iright][jup].y, points[0][iright][jup].z);
				glColor3f(points[0][i][jup].z / max, 0, 0);
				glVertex3d(points[0][i][jup].x, points[0][i][jup].y, points[0][i][jup].z);
			}
		}
		glEnd();
		/*glBegin(GL_LINES);
		glColor3d(1.0, 0, 0);
		glVertex3d(1,0,1)
		glEnd();*/
		glFinish();
		SwapBuffers(wglGetCurrentDC());
	}
}

void Paint3D::InitiateOPGL(CRect & rt, CDC* pdc)
{
	rect = rt;
	pDC = pdc;
	HGLRC hrc; //rendering context for OpGL(�������������� �������� ��� OpGl)
	if (!bSetupPixelFormat())
	{
		return;
	}

	hrc = wglCreateContext(pDC->GetSafeHdc()); //������������� ��� ����������� cdc
	ASSERT(hrc != NULL); //���� �������� ������������������, �� ���������� ���������, ����� ����������� ����������

	wglMakeCurrent(pDC->GetSafeHdc(), hrc); //��������� ��������� �������� ���������

	glViewport(0, 0, rect.right, rect.bottom);

	glCullFace(GL_FRONT);

	//glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0.0, 0.0, 0.0, 1.0);


}

BOOL Paint3D::bSetupPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),  // size of this pfd
		1,                              // version number
		PFD_DRAW_TO_WINDOW |            // support window
		PFD_SUPPORT_OPENGL |          // support OpenGL
		PFD_DOUBLEBUFFER,             // double buffered
		PFD_TYPE_RGBA,                  // RGBA type
		24,                             // 24-bit color depth
		0, 0, 0, 0, 0, 0,               // color bits ignored
		0,                              // no alpha buffer
		0,                              // shift bit ignored
		0,                              // no accumulation buffer
		0, 0, 0, 0,                     // accum bits ignored
		32,                             // 32-bit z-buffer
		0,                              // no stencil buffer
		0,                              // no auxiliary buffer
		PFD_MAIN_PLANE,                 // main layer
		0,                              // reserved
		0, 0, 0                         // layer masks ignored
	};
	int pixelformat;
	if ((pixelformat = ChoosePixelFormat(pDC->GetSafeHdc(), &pfd)) == 0)
	{
		return FALSE;
	}

	if (SetPixelFormat(pDC->GetSafeHdc(), pixelformat, &pfd) == FALSE)
	{
		return FALSE;
	}
	return TRUE;
}