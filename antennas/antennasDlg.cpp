
// antennasDlg.cpp: файл реализации
//

#include "stdafx.h"
#include "antennas.h"
#include "antennasDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define M_PI   3.141592653589793238462643


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CantennasDlg
CRITICAL_SECTION cs;



CantennasDlg::CantennasDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ANTENNAS_DIALOG, pParent)
	, d(1)
	, R(1000)
	, lamda(3)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CantennasDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ANT, Pic_ant);
	DDX_Text(pDX, IDC_EDIT3, d);
	DDX_Text(pDX, IDC_EDIT1, R);
	DDX_Text(pDX, IDC_EDIT2, lamda);
	DDX_Control(pDX, IDC_2D, Pic_2d);
}

BEGIN_MESSAGE_MAP(CantennasDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_STN_CLICKED(IDC_ANT, &CantennasDlg::OnStnClickedAnt)
	ON_BN_CLICKED(IDOK, &CantennasDlg::OnBnClickedOk)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_BUTTON2, &CantennasDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &CantennasDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON1, &CantennasDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CantennasDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// Обработчики сообщений CantennasDlg

BOOL CantennasDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	InitializeCriticalSection(&cs);
	GetDlgItem(IDC_ANT)->GetWindowRect(&rectAntenns);
	xmax = 10;
	xmin = -10;
	ymax = 10;
	ymin = -10;
	Pic_ant.xmax = xmax;
	Pic_ant.xmin = xmin;
	Pic_ant.ymax = ymax;
	Pic_ant.ymin = ymin;
	Pic_2d.image = &points2D;

	CDC* dc = GetDlgItem(IDC_3D)->GetDC();
	GetDlgItem(IDC_3D)->GetClientRect(&PicSurface);
	Pic_3D.InitiateOPGL(PicSurface, dc);
	angleX = 50;
	angleY = 0;
	zoom = 1;
	Pic_3D.angleX = &angleX;
	Pic_3D.angleY = &angleY;
	Pic_3D.zoom = &zoom;
	Pic_3D.points = &maspoints;
	oldPoint.x = 0;
	oldPoint.y = 0;


	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CantennasDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CantennasDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CantennasDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//нажатие антены
void CantennasDlg::OnStnClickedAnt()
{
	UpdateData(TRUE);
	points = Pic_ant.points;
	for (size_t i = 0; i < points.size(); i++)
	{
		points[i].X *= d;
		points[i].Y *= d;

	}
	Initializepoints();
	Initialize3Dpoints();
	Generatepoints();
	Initialize2Dpoints();
	Pic_2d.RedrawWindow();
	SetTimer(1, 30, NULL);
}


void CantennasDlg::OnBnClickedOk()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	points = Pic_ant.points;
	update_points();
	Initializepoints();
	Initialize3Dpoints();
	Generatepoints();
	Initialize2Dpoints();
	Pic_2d.RedrawWindow();
	//SetTimer(1, 30, NULL);

}
void CantennasDlg::update_points()
{
	for (size_t i = 0; i < points.size(); i++)
	{
		points[i].X *= d;
		points[i].Y *= d;

	}
}

void CantennasDlg::Initialize2Dpoints()
{
	points2D.clear();
	size_t width = maspoints[0].size(),
		height = maspoints.size();
	for (size_t i = 0; i < height; i++)
	{
		std::vector<double> buf;
		for (size_t j = 0; j < width; j++)
		{
			buf.push_back(maspoints[i][j].z);
		}
		points2D.push_back(buf);
	}
}


//инициализация плоскости
void CantennasDlg::Initializepoints()
{
	maspoints.clear();
	std::vector<Point3D> buffer;
	const size_t width = 1000, height = 1000;
	Point3D buf;
	buf.x = 0;
	buf.y = 0;
	buf.z = 0;
	buffer.resize(width, buf);
	maspoints.resize(height, buffer);
}

void CantennasDlg::Initialize3Dpoints()
{
	int height = maspoints.size() - 1,
		width = maspoints[0].size() - 1;

	for (int i = 0; i <= height; i++)
	{
		for (int j = 0; j <= width; j++)
		{
			Point3D pt;
			double scal = (double)R / ((double)width - 1.0)*2.0;
			pt.x = (double)((j - width / 2))*scal;
			pt.y = (double)((i - height / 2))*scal;
			if ((pt.x*pt.x + pt.y*pt.y) >= R * R) pt.z = 0;
			else
			{
				pt.z = sqrt(R*R - pt.x*pt.x - pt.y*pt.y);
			}
			maspoints[i][j] = pt;
		}
		double k = 0;
	}
}

void CantennasDlg::Generatepoints()//распределение
{
	int height = maspoints.size() - 1,
		width = maspoints[0].size() - 1;
	for (int i = 0; i <= height; i++)
	{
		for (int j = 0; j <= width; j++)
		{
			if (maspoints[i][j].z > 0 && !points.empty())
			{
				double re = 0, im = 0;
				for (int antenna = 0; antenna < points.size(); antenna++)
				{
	
					double moduleR = sqrt((maspoints[i][j].x - (double)points[antenna].X)*(maspoints[i][j].x - (double)points[antenna].X) + (maspoints[i][j].y - (double)points[antenna].Y)*(maspoints[i][j].y - (double)points[antenna].Y) + (maspoints[i][j].z)*(maspoints[i][j].z));
					re += cos((double)2.0*M_PI / lamda * moduleR) / moduleR;
					im += -sin((double)2.0*M_PI / lamda * moduleR) / moduleR;
				}
				maspoints[i][j].z = sqrt(re*re + im * im);
			}
		}
	}
}
void CantennasDlg::OnTimer(UINT_PTR nIDEvent)
{
	EnterCriticalSection(&cs);
	//if (_isAnimate)	angleY += 2;
	//if (msg->message == WM_KEYDOWN)
	//{
	//	if (msg->wParam == VK_UP)
	//	{
	//		AfxMessageBox(L"Нажалит стрелку вверх", MB_OK);
	//	}
	//	if (msg->wParam == VK_DOWN)
	//	{
	//		AfxMessageBox(L"Нажата стрелка вниз", MB_OK);
	//	}
	//}



	Pic_3D.Draw();
	LeaveCriticalSection(&cs);

	CDialog::OnTimer(nIDEvent);
}

void CantennasDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	switch (nChar)
	{
	case 'a':
		R = 1;
		UpdateData(false);
	default:

		break;
	}
	CDialogEx::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CantennasDlg::OnBnClickedButton2()
{
	angleX -= 10;

}


void CantennasDlg::OnBnClickedButton4()
{

	angleX += 10;
}


void CantennasDlg::OnBnClickedButton1()
{
	angleY -= 10;

}


void CantennasDlg::OnBnClickedButton3()
{
	angleY += 10;

}
