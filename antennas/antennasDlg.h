#include <vector>
#include "Paint.h"
#include <gdiplus.h>
using namespace Gdiplus;
using namespace std;
// antennasDlg.h: файл заголовка
//

#pragma once

// Диалоговое окно CantennasDlg
class CantennasDlg : public CDialogEx
{
// Создание
public:
	CantennasDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ANTENNAS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double ymax, ymin, xmax, xmin;
	std::vector<Point> points;
	std::vector<std::vector<Point3D>> maspoints;
	std::vector<std::vector<double>> points2D;
	CRect rectAntenns;
	afx_msg void OnStnClickedAnt();
	afx_msg void OnBnClickedOk();
	void update_points();
	void Initialize2Dpoints();
	void Initializepoints();
	void Initialize3Dpoints();
	void Generatepoints();
	void OnTimer(UINT_PTR nIDEvent);
	Paint Pic_ant;
	double d;	
	CPoint oldPoint;
	double angleX, angleY, zoom;
	CRect PicSurface;
	int R;
	double lamda;
	Paint2D Pic_2d;
	Paint3D Pic_3D;
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
};
